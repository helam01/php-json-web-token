<?php

class User {


	private function JWTGen($user) {
		$header = array('alg'=>'HS256', 'typ'=>'JWT');

		$payload = $user;

		$secretKey = 'ESAdsda2345ds25dFs42D6g4d7DFsesD5s6r5289fH';

		$header_hash = base64_encode( json_encode($header) );
		$payload_hash = base64_encode( json_encode($payload) );

		$signature = hash_hmac(
			'sha256',
			$header_hash . "." . $payload_hash,
			$secretKey
		);

		$jwt = $header_hash.".".$payload_hash.".".$signature;

		return $jwt;
	}



	private function tokenRegister( $jwt, $user_id ) {
		$tokenLog = array(
			"token" => $jwt,
			"user" => $user_id,
			"date" => date('Y-m-d H:i:s'),
			"status" => "active"
		);

		return true;
	}


	private function checkToken( $token ) {
		$registers = array(
			array(
				"token" 	=> "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySUQiOiIxMzIxMzIxMzIiLCJub21lIjoiSGVsYW0gTW9yZWlyYSIsImVtYWlsIjoiaGVsYW1AaG9yaXpvbm5ldy5jb20uYnIiLCJ0b2tlbkNvbnRyb2wiOiIwMGJhN2Q1NjhkYWU1YWEyMzRhZjAxYmYyODEzZTAxNyJ9.66210c70451a0d97b48d9fcf45d621ceef7e972cd5e75083938be32491475525",
				"user_id" 	=> "132132132",
				"date" 		=> '2015-10-29 15:35:12',
				"status" => "active"
			)
		);


		$token_validate = false;
		for ( $x=0; $x < count($registers); $x ++ ) {
			if ( $registers[$x]['token'] == $token && $registers[$x]['status'] == "active" ) {
				$token_validate = true;
				break;
			}
		}

		return $token_validate;
	}



	public function login() {
		if ( !isset($_POST['username']) && !isset($_POST['passwd']) ) {
			$response = array('status'=>'FAIL', 'message'=>'Please, inform username and password');
			die ( json_encode($response) );
		}

		$username = $_POST['username'];
		$passwd = $_POST['passwd'];		

		if ( $username != "helam@horizonnew.com.br" || $passwd != "1234") {
			$response = array('status'=>'FAIL', 'message'=>'Invalid username or password');
			header('HTTP/1.0 401 Unauthorized');
			die ( json_encode($response) );
		}

		$payload = array(
			'userID'=>'132132132',  
			'nome'=> 'Helam Moreira', 
			'email'=>$username,
			'tokenControl' => md5(uniqid())
		);

		$jwt = $this->JWTGen( $payload );

		$response = array('status'=>'OK', 'message'=>'user logged', 'content'=>$jwt);

		die ( json_encode($response) );
	}


	public function listar() {
		if ( !$this->checkToken( $_GET['token'] ) ) {
			$response = array('status'=>'FAIL', 'message'=>'Invalid token');
			header('HTTP/1.0 401 Unauthorized');
			die ( json_encode($response) );
		}


		$response = array('status'=>'OK', 'message'=>'Users list');
		die ( json_encode($response) );

	}



	public function index() {
		$response = array('status'=>'OK', 'message'=>'User Index');
		die ( json_encode($response) );
	}

}