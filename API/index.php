<?php


$page = ( isset($_GET['pag']) ) ? $_GET['pag'] : 'index';
$action = ( isset($_GET['action']) ) ? $_GET['action'] : 'index';


if ( $page !=  "index" ) {
	include $page .".php";
	$controller = new $page();
	$controller->$action();
}


die(json_encode( array('status'=>'OK', 'message'=> "API Page" ) ));